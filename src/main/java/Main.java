import client.GroceryClient;
import client.GroceryClientException;
import model.ItemList;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import service.FruitScraperImpl;
import service.ProduceScraperService;
import util.URIHelper;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static final String PRODUCT_GRID_CLASSNAME = "productLister gridView";
    public static final Scanner KEY_SCANNER = new Scanner(System.in);
    private static Document BASE_PAGE;
    static GroceryClient groceryClient = new GroceryClient();
    private static ProduceScraperService produceScraperService = new FruitScraperImpl(groceryClient);

    public static void main (String [] args) throws GroceryClientException, URISyntaxException {
        displayMenu();
        int menuVal = KEY_SCANNER.nextInt();
        displayMenuOtions(menuVal);
    }

    private static void displayMenuOtions(int menuVal) throws GroceryClientException, URISyntaxException {
        switch(menuVal){
            case 1:
                getAllProductsJsonResponse();
                displayMenu();
                menuVal = KEY_SCANNER.nextInt();
                displayMenuOtions(menuVal);
                break;
            case -1:
                System.exit(-1);
                break;
            default:
                displayMenu();
                displayMenuOtions(menuVal);
                break;
        }
    }

    private static void getAllProductsJsonResponse() throws GroceryClientException, URISyntaxException {
        Document mainPageDocument = groceryClient.getPageHtmlAsync(URIHelper.SAINSBURY_GROCERY_HOME_URL).block();
        Elements basePageElements = produceScraperService.retrieveProductGridFromSearchPageByClassId(PRODUCT_GRID_CLASSNAME, mainPageDocument);
        List<Element> gridIdToGridElement = produceScraperService.getProductGridElements(basePageElements);
        Map<String, String> descriptionHrefMap = produceScraperService.getExtractedGridItemUrlMap(gridIdToGridElement);
        ItemList derivedItemList = produceScraperService.generateCalculatedItemListResponse(descriptionHrefMap);
        System.out.println(derivedItemList.createJsonString());
    }

    public static void displayMenu(){
        System.out.println("Please select an option\n");
        System.out.println("1. Produce JSON response from Sainsbury's grocery site.");
        System.out.println("2. Enter -1 to exit.");
    }
}
