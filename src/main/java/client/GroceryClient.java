package client;

import lombok.NonNull;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;
import reactor.ipc.netty.http.client.HttpClient;

import java.io.IOException;

public class GroceryClient implements Crawler {

    private static Logger logger = LoggerFactory.getLogger(GroceryClient.class);

    public Mono<Document> getPageHtmlAsync(@NonNull String url) throws GroceryClientException{
        HttpClient httpClient = HttpClient.create();
        try {
            Mono<Document> pageExtractedHtml = Mono.just(Jsoup.parse(
                    httpClient.get(url).block().receive().asString().toStream().reduce((s, s2) -> s + s2).get()));
            return pageExtractedHtml;
        }catch(Exception ex ){
            logger.error("Error while downloading document: ", ex);
            throw new GroceryClientException("Sorry an error occurred while downloading document");
        }

    }


    public Document getPageHtml(@NonNull String url) {
        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            logger.debug("Error while retrieving HTML from URL", e);
            throw new IllegalStateException(e);
        }
    }

}
