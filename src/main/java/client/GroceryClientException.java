package client;

public class GroceryClientException extends Exception {
    public GroceryClientException(String message) {
        super(message);
    }
}
