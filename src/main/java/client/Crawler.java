package client;

import org.jsoup.nodes.Document;

public interface Crawler {

    public Document getPageHtml(String url);
}
