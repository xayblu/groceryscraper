package service;

import client.GroceryClient;
import client.GroceryClientException;
import model.Fruit;
import model.GroceryItem;
import model.ItemList;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import util.URIHelper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FruitScraperImpl implements ProduceScraperService {

    private static final String KCAL_TEXT_EXTRACTION_PATTERN = "(.\\d+)(kcal)(.*)";
    private static final String PRICE_STRING_CLEANUP_REGEX = "[^\\x00-\\x7F]";
    private static final String DIV_PRODUCT_TITLE_CSS_SELECTOR = "div.productTitleDescriptionContainer";
    private static final String DIV_PRODUCT_TEXT_CSS_SELECTOR = "div.productText";
    private static final String RICE_PER_UNIT_CSS_SELECTOR = "p.pricePerUnit";
    private static Logger logger = LoggerFactory.getLogger(GroceryClient.class);

    private static GroceryClient groceryClient = new GroceryClient();

    public FruitScraperImpl(GroceryClient groceryClient) {
        this.groceryClient = groceryClient;
    }

    @Override
    public Elements retrieveProductGridFromSearchPageByClassId(String classId, Document BASE_GROCERY_PAGE_DOCUMENT) {
        return BASE_GROCERY_PAGE_DOCUMENT.getElementsByClass(classId);
    }

    @Override
    public List<Element> getProductGridElements(Elements productGridElements) {
      Elements gridElements =  productGridElements.select("div."+"productNameAndPromotions");
      return gridElements.stream().collect(Collectors.toList());
    }

    @Override
    public Map<String, String> getExtractedGridItemUrlMap(List<Element> gridIdToGridElement) {
    Map concurrentDescToUrlMap = new ConcurrentHashMap();

        gridIdToGridElement.forEach(gridElement -> concurrentDescToUrlMap.put(URIHelper.createGroceryProductInformationUTI(gridElement.select("a"
        ).attr("href")), gridElement.select("a").text()));

        return concurrentDescToUrlMap;
    }

    @Override
    public Flux<GroceryItem> createProductListFromGridItems(Map<String, String> descriptionHrefMap) {
       Flux<String> urlList = Flux.fromIterable(descriptionHrefMap.keySet());
       Flux<GroceryItem> fruitList = createFruitItemDocumentList(urlList.toStream().collect(Collectors.toList()));
        return fruitList;
    }

    @Override
    public ItemList generateCalculatedItemListResponse(Map<String, String> descriptionHrefMap) {
        List<GroceryItem> groceryItemList = createProductListFromGridItems(descriptionHrefMap)
                .toStream().collect(Collectors.toList());

        ItemList calculatedItemList = ItemList.builder().groceryItemList(groceryItemList).build().calculateTotal();

        return calculatedItemList;
    }

    private Flux<GroceryItem> createFruitItemDocumentList(List<String> productUrl) {
        List<Document> documentList = new ArrayList<>();
        List<GroceryItem> groceryItemList = new ArrayList<>();
        Flux.fromIterable(productUrl).doOnEach(itemUrl ->{
            if(itemUrl.get() != null) {
                try {
                    documentList.add(groceryClient.getPageHtmlAsync(itemUrl.get()).block());
                } catch (GroceryClientException e) {
                   logger.error("Error while attempting to download product grid dom elements:");
                }
            }
        }).parallel().subscribe();


        Flux.fromIterable(documentList).doOnEach(doc -> {
            if(doc.get() != null)
                groceryItemList.add(createFruitFromDom(doc.get()));

        }).blockLast();
        return Flux.fromIterable(groceryItemList);
    }

    private GroceryItem createFruitFromDom(Document productGridDocument) {
        String descText = getProductDescription(productGridDocument);
        String productTitleText = getProductTitleFromProduct(productGridDocument);
        String productKcalPer100g = getProductKcalPer100GFromDocument(productGridDocument);
        String productUnitPrice = getProductUnitPriceFromDocument(productGridDocument);

        GroceryItem extractedGroceryItem = new Fruit(productTitleText, productKcalPer100g,
                BigDecimal.valueOf(Double.valueOf(productUnitPrice)).setScale(2, RoundingMode.HALF_EVEN),
                descText);
        return extractedGroceryItem;
    }

    private String getProductUnitPriceFromDocument(Document productGridDocument) {
        String unitPriceText = productGridDocument.select(RICE_PER_UNIT_CSS_SELECTOR).text();
        String[] unitPriceSplit = unitPriceText.split("/");
        String unitPriceExtract = unitPriceSplit[0].replaceAll(PRICE_STRING_CLEANUP_REGEX, "");
        return unitPriceExtract;
    }

    private String getProductKcalPer100GFromDocument(Document productGridDocument) {
        String kcalValue = getKCalDocumentQuery(productGridDocument);

        Pattern p = Pattern.compile(KCAL_TEXT_EXTRACTION_PATTERN);
        Matcher m = p.matcher(kcalValue);
        String extractedKcalValue = m.matches() ? m.group(1) : null;
        return extractedKcalValue;
    }

    private String getKCalDocumentQuery(Document productGridDocument) {

        String kcalValueCssQuery1 = productGridDocument.select("td.nutritionLevel1").text();
        String kcalValueCssQuery2 = productGridDocument.select("tr.tableRow0").select("td.tableRow0").text();
        String kcalValueCssQueryResult =  kcalValueCssQuery2.length() >= kcalValueCssQuery1.length() ? kcalValueCssQuery2 : kcalValueCssQuery1;

        return kcalValueCssQueryResult;
    }

    private String getProductTitleFromProduct(Document productGridDocument) {
        String productTitle = productGridDocument.select(DIV_PRODUCT_TITLE_CSS_SELECTOR).text();
        return URIHelper.decodeUnescapedStringFromHtmlDocument(productTitle);
    }

    private String getProductDescription(Document productGridDocument) {
        String productDescription = productGridDocument.select(DIV_PRODUCT_TEXT_CSS_SELECTOR).select("p").first().text();
        return URIHelper.decodeUnescapedStringFromHtmlDocument(productDescription);
    }


}
