package service;

import model.GroceryItem;
import model.ItemList;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Map;

public interface ProduceScraperService {

    Elements retrieveProductGridFromSearchPageByClassId(String classId, Document BASE_GROCERY_PAGE_DOCUMENT);

    List<Element> getProductGridElements(Elements productGridElements);

    Map<String, String> getExtractedGridItemUrlMap(List<Element> gridIdToGridElement);

    Flux<GroceryItem> createProductListFromGridItems(Map<String, String> decriptionHrefMap);

    ItemList generateCalculatedItemListResponse(Map<String, String> descriptionHrefMap);
}



