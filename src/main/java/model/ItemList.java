package model;

import client.GroceryClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.GroceryCalculatory;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class ItemList{
    private List<GroceryItem> groceryItemList;
    private Total total;

    private static Logger logger = LoggerFactory.getLogger(GroceryClient.class);

    public ItemList calculateTotal(){
        try {
            BigDecimal gross = groceryItemList.stream().map(groceryItem ->
                    groceryItem.getUnit_price()).reduce((unitPrice, unitPrice2) -> unitPrice.add(unitPrice2)).get();
            Total calculatedTotal = Total.builder().GROSS(gross).VAT(GroceryCalculatory.calculateGroceryVAT(gross)).build();
            this.total = calculatedTotal;
        }catch (Exception ex){
            logger.debug("Error while calculating VAT tax.", ex.getMessage());
        }
        return new ItemList(groceryItemList, total);
    }

    public String createJsonString(){
        Gson jsonParser = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
        String jsonString = jsonParser.toJson(this);

        return jsonString;
    }

}
