package model;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@EqualsAndHashCode
public class Fruit extends GroceryItem {

    public Fruit(String title, String kcal_per_100G, BigDecimal unit_price, String description) {
        super(title, kcal_per_100G, unit_price, description);
    }
}
