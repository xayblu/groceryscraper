package model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public abstract class GroceryItem{
    private String title;
    private String kcal_per_100G;
    private BigDecimal unit_price;
    private String description;
}

