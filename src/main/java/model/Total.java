package model;

import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class Total {
    private BigDecimal VAT;
    private BigDecimal GROSS;
}
