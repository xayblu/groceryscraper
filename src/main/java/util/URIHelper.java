package util;

import lombok.NonNull;
import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class URIHelper {

    private static Logger logger = LoggerFactory.getLogger(URIHelper.class);

    public static final String SAINSBURY_GROCERY_BASE_URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/";
    public static final String SAINSBURY_GROCERY_HOME_URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";

    public static String createGroceryProductInformationUTI(@NonNull String uriInputString) {
        String productInfromationUri = SAINSBURY_GROCERY_BASE_URL.concat(uriInputString.replace("../",""));
        return productInfromationUri;
    }

    public static String decodeUnescapedStringFromHtmlDocument(@NonNull String unescapedHtmlText){
        String extractedText = StringEscapeUtils.unescapeEcmaScript(unescapedHtmlText);
        return extractedText;
    }
}
