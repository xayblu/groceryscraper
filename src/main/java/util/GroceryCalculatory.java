package util;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import model.GroceryItem;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

import static java.math.BigDecimal.ZERO;

@Slf4j
public class GroceryCalculatory {

    private static BigDecimal STANDARD_VAT = new BigDecimal(1.2);


    public static BigDecimal calculateGroceryVAT(@NonNull BigDecimal gross) {
        if (gross.compareTo(ZERO) > 0 ) {
            BigDecimal netPrice = gross.divide(STANDARD_VAT, 2, RoundingMode.HALF_EVEN);
            return gross.subtract(netPrice);
        }
        throw new IllegalArgumentException("Gross must be greater than 0!");
    }

    public static BigDecimal calculateGroceryGrossPrice(@NonNull List<GroceryItem> fruitList) {
        List<BigDecimal> unitPriceList = fruitList.stream().map(GroceryItem::getUnit_price).collect(Collectors.toList());
        BigDecimal gross = unitPriceList.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
        return gross;
    }
}
