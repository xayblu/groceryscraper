# Project Title

Sainsbury grocery website scraper
This project aims to automate the scraping of the Sainsbury grocery website for nutrional, price and description
information on berries, cherries and currants

### Prerequisites
Java 8 or higher
Maven

### Installing
Import using your favorite IDE as a maven project and run a clean install
Alternatively you can navigate to the project root directory and run the following command:
javac -classpath . \src\Main.java

End with an example of getting some data out of the system or using it for a little demo

## Running the tests
If using Intellij or any other IDE import the project as a maven project and add the maven run config below
test -f pom.xml
or if using the command line navigate to the root directory of the project and run 
mvn test -f pom.xml


## Built With

* [JSOUP](https://jsoup.org/) - The html parser used
* [Maven](https://maven.apache.org/) - Dependency Management
* [GSON](https://sites.google.com/site/gson/gson-user-guide) - Used to deserialize POJOs
* [Lombok](https://projectlombok.org/) - Used for builders, immutable objects and toString/EqualsHash
* [LogBack](https://logback.qos.ch/) - Used for logging
* [Project Reactor](https://projectreactor.io/) - Used for NIO http calls for async downloads
* [Junit 5](https://junit.org/junit5/) - Used for test suite




## Authors

* **Xavier Simms**
