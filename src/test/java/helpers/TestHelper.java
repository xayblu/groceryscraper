package helpers;

import client.GroceryClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import service.ProduceScraperService;
import service.FruitScraperImpl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TestHelper {


    public static final String BERRY_CHERRY_CURRANT_BASE_URL = "https://jsainsburyplc.github.io/serverside-test/" +
            "site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries" +
            "/berries-cherries-currants6039.html";

    public static Document  BASE_GROCERY_PAGE_DOCUMENT;
    private static Elements productGridElements;
    public static final String PRODUCT_GRID_CLASSNAME = "productLister gridView";
    public static final String PRODUCT_GRID_ELEMNTS = "gridItem";
    public static ProduceScraperService produceScraperService;
    private static TestHelper testHelper;
    private static GroceryClient groceryClient = new GroceryClient();



    public static TestHelper getInstance(){
        if(testHelper == null){
            produceScraperService = new FruitScraperImpl(groceryClient);
            testHelper = new TestHelper();
        }
        return testHelper;
    }

    public static Document getBaseGroceryPageDocument() throws IOException {
        return Jsoup.parse(getSainsburyFruitBasePageAsString());
    }
    public static String  getSainsburyFruitBasePageAsString() throws IOException {
        ClassLoader classLoader = TestHelper.class.getClassLoader();
        File file = new File(classLoader.getResource("baseTest.html").getFile());
        byte[] encoded = Files.readAllBytes(Paths.get(file.getPath()));
        return new String(encoded, "UTF-8");
    }
}
