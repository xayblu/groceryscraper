package service;

import client.GroceryClient;
import client.GroceryClientException;
import helpers.TestHelper;
import model.GroceryItem;
import model.ItemList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import util.URIHelper;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

class testObjImplTest {

    public static final int NUMBER_OF_ITEMS = 17;
    public static final int EXPECTED_NUMBER_OF_GRID_ITEMS = 17;
    public static final String EXPECTED_KCAL = "33kcal";

    private Document BASE_GROCERY_PAGE_DOCUMENT;
    private ProduceScraperService testObj;
    private GroceryClient groceryClient = new GroceryClient();
    private Elements productGridElements;
    public static final String PRODUCT_GRID_CLASSNAME = "productLister gridView";



    @BeforeEach
    void setUp() throws IOException {
        BASE_GROCERY_PAGE_DOCUMENT = Jsoup.parse(TestHelper.getSainsburyFruitBasePageAsString());
        testObj = new FruitScraperImpl(groceryClient);
        productGridElements = testObj.retrieveProductGridFromSearchPageByClassId(PRODUCT_GRID_CLASSNAME, BASE_GROCERY_PAGE_DOCUMENT);
    }


    @Test
    @DisplayName("Testing getting grid element from base page")
    public void testingRetrievalOfProductGridFromBasePage() throws IOException {
        Elements gridElement = testObj.retrieveProductGridFromSearchPageByClassId(PRODUCT_GRID_CLASSNAME, BASE_GROCERY_PAGE_DOCUMENT);
        assertNotNull(gridElement);
    }

    @Test
    @DisplayName("Testing retreival of individual grid elements DOM")
    public void testGetProductGridElements(){
        List<Element> gridIdToGridElement = testObj.getProductGridElements(productGridElements);
        assertNotNull(gridIdToGridElement);
        assertEquals(EXPECTED_NUMBER_OF_GRID_ITEMS, gridIdToGridElement.size());
    }

    @Test
    @DisplayName("Testing the extraction of product description and url from href tags")
    public void testGetExtractedGridInfo(){
        Map<String, String> decriptionHrefMap;
        List<Element> gridIdToGridElement = testObj.getProductGridElements(productGridElements);
        decriptionHrefMap = testObj.getExtractedGridItemUrlMap(gridIdToGridElement);
        assertEquals(decriptionHrefMap.size(), gridIdToGridElement.size());
    }

    @Test
    @DisplayName("Testing the retreival of item details list")
    public void testShouldCreateListOfFruitObjectsWithPopulatedDetails(){
        Map<String, String> decriptionHrefMap;
        List<Element> gridIdToGridElement = testObj.getProductGridElements(productGridElements);
        decriptionHrefMap = testObj.getExtractedGridItemUrlMap(gridIdToGridElement);
        Flux<GroceryItem> actual = testObj.createProductListFromGridItems(decriptionHrefMap);
        assertTrue(actual.hasElements().block());
    }

    @Test
    @DisplayName("Testing the calculation of total and gross based on item retrieval")
    void generateCalculateItemListResponse() throws GroceryClientException {
        Document mainPageDocument = groceryClient.getPageHtmlAsync(URIHelper.SAINSBURY_GROCERY_HOME_URL).block();
        Elements basePageElements = testObj.retrieveProductGridFromSearchPageByClassId(PRODUCT_GRID_CLASSNAME, mainPageDocument);
        List<Element> gridIdToGridElement = testObj.getProductGridElements(basePageElements);
        Map<String, String> descriptionHrefMap = testObj.getExtractedGridItemUrlMap(gridIdToGridElement);
        ItemList actual = testObj.generateCalculatedItemListResponse(descriptionHrefMap);

        assertTrue(descriptionHrefMap.size() == NUMBER_OF_ITEMS);
        assertEquals(NUMBER_OF_ITEMS, actual.getGroceryItemList().size());

    }

    @Test
    public void testUnitPriceExtraction(){
        String unitPirceIn = "£2.50/unit £2.50/unit";
        String[] unitPriceSplit = unitPirceIn.split("/");
        String BigDecimal = unitPriceSplit[0].substring(1);
    }

    @Test
    public void testKcalExtraction(){
        String testInput = "33kcal 2% <0.5g - <0.1g - <0.01g -";
        Pattern p = Pattern.compile("(.\\d+)(kcal)(.*)");
        Matcher m = p.matcher(testInput);
    }
}