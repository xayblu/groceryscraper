package util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class URIHelperTest {

    private static final String URI_INPUT_STRING =
            "../../../../../../shop/gb/groceries/berries-cherries-currants/sainsburys-blueberries-200g.html";
    private static final String EXPECTED_URI =
            "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk" +
            "/shop/gb/groceries/berries-cherries-currants/sainsburys-blueberries-200g.html";
    private static final String EXTRACTED_HTML_TEXT = "Sainsbury\\u0027s Blueberries 200g";
    private static final String EXPECTED_DECODED_EXTRACTED_HTML_TEXT = "Sainsbury's Blueberries 200g";

    @Test
    @DisplayName("Testing product URI extraction from href html tag")
    public void testUriExtractionFromHrefTag(){
        String actualOutPut = URIHelper.createGroceryProductInformationUTI(URI_INPUT_STRING);
        assertEquals(EXPECTED_URI, actualOutPut);
    }

    @Test
    @DisplayName("Testing the decoding of escaped HTML text to UTF-8")
    public void testEscapedHtmlTextToUTF8(){
        String actual = URIHelper.decodeUnescapedStringFromHtmlDocument(EXTRACTED_HTML_TEXT);
        assertEquals(EXPECTED_DECODED_EXTRACTED_HTML_TEXT, actual);
    }

}