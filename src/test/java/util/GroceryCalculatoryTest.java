package util;

import model.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Testing Grocery Calculator Utility")
class GroceryCalculatoryTest {
    private static final BigDecimal EXPECTED_VAT = BigDecimal.valueOf(0.83);
    private static final BigDecimal EXPECTED_GROSS = BigDecimal.valueOf(5.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
    private static final Total EXPECTED_TOTAL = Total.builder().GROSS(EXPECTED_GROSS).VAT(EXPECTED_VAT).build();
    private static final ItemList EXPECTED_OUTPUT = new ItemList(populateFruitList(), EXPECTED_TOTAL);
    public static final String GROSS_MUST_BE_GREATER_THAN_0 = "Gross must be greater than 0!";

    @Test()
    @DisplayName("Testing illegal argument exception for gross price of 0 or less")
    public void testShouldThrowIllegalArgumentExceptionForGrossZeroOrLess(){
        Throwable exception = assertThrows(IllegalArgumentException.class, () ->
            GroceryCalculatory.calculateGroceryVAT(BigDecimal.valueOf(0)));

        assertEquals(GROSS_MUST_BE_GREATER_THAN_0, exception.getMessage());
    }

    @Test
    @DisplayName("Testing to calculate the VAT based on gross price")
    public void testCalculationForVAT(){
        BigDecimal actualVAT = GroceryCalculatory.calculateGroceryVAT(EXPECTED_GROSS);
        assertEquals(EXPECTED_VAT, actualVAT);
    }

    @Test
    @DisplayName("Testing to calculate and populate total object")
    public void testToCalculateTotalAndVATFromGross(){
        BigDecimal VAT = GroceryCalculatory.calculateGroceryVAT(EXPECTED_GROSS);
        Total actualTotal = Total.builder().GROSS(EXPECTED_GROSS).VAT(VAT).build();
        assertEquals(EXPECTED_TOTAL, actualTotal);
    }

    @Test
    @DisplayName("Testing to calculate the gross price based on fruits in the list")
    public void testToCalculateTheGrossPriceFromAListOfFruits(){
        BigDecimal actualGrossPrice = GroceryCalculatory.calculateGroceryGrossPrice(populateFruitList());
        assertEquals(EXPECTED_GROSS, actualGrossPrice);
    }

    private static List<GroceryItem> populateFruitList(){
        GroceryItem blackBerries = new Fruit("Fresh Black Fruit", "60", BigDecimal.valueOf(1.75), "Organic Black Fruit");
        GroceryItem organicCherries = new Fruit("Italian Cherries", "23", BigDecimal.valueOf(1.75), "Italian Red Cherries");
        GroceryItem strawberries = new Fruit("Large Strawberries", "55", BigDecimal.valueOf(1.5), "Large Strawberries");
        List<GroceryItem> groceryItemList = Arrays.asList(blackBerries, organicCherries, strawberries);
        return groceryItemList;
    }

}