package client;

import helpers.TestHelper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import service.FruitScraperImpl;
import service.ProduceScraperService;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GroceryClientTest {

    private static final String BERRY_CHERRY_CURRANT_BASE_URL = "https://jsainsburyplc.github.io/serverside-test/" +
            "site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries" +
            "/berries-cherries-currants6039.html";

    private static final String BLUEBERRY_PRODUCT_INFO =
            "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop" +
                    "/gb/groceries/berries-cherries-currants/sainsburys-blueberries-200g.html";


    private Document BASE_GROCERY_PAGE_DOCUMENT;
    private GroceryClient testObj = new GroceryClient();
    private ProduceScraperService produceScraperService;
    private Elements productGridElements;
    public static final String PRODUCT_GRID_CLASSNAME = "productLister gridView";
    public static final String PRODUCT_GRID_ELEMNTS = "gridItem";


    @BeforeEach
    void setUp() throws IOException {
        BASE_GROCERY_PAGE_DOCUMENT = Jsoup.parse(TestHelper.BERRY_CHERRY_CURRANT_BASE_URL);
        produceScraperService = new FruitScraperImpl(testObj);
        productGridElements = produceScraperService.retrieveProductGridFromSearchPageByClassId(PRODUCT_GRID_CLASSNAME, BASE_GROCERY_PAGE_DOCUMENT);
    }


    @Test
    @DisplayName("Testing inner HTML is retreived from url string")
    void getPageHTML() {
        Document actualHtmlText =
                testObj.getPageHtml(BERRY_CHERRY_CURRANT_BASE_URL);
        assertNotNull(actualHtmlText);
    }


    @Test
    @DisplayName("Testing non blocking implementation for individual product info page retrieval")
    void getPageHtmlAsync() throws GroceryClientException {
        Mono<Document> productInfoPage = testObj.getPageHtmlAsync(BLUEBERRY_PRODUCT_INFO);
        Document actualProductPage = productInfoPage.block();
        assertNotNull(actualProductPage);
    }

    @Test
    @DisplayName("Testing grocery client throws exception on bad URL")
    void getPageHtmlAsyncShouldThrowExceptionOnBadURL(){
        assertThrows(GroceryClientException.class,
                () -> testObj.getPageHtmlAsync("fakedate").block());
    }

}